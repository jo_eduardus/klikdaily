const { trstocks,stocks } = require('../models');

module.exports = {

    indexStocksAPI: (req, res) => {

        stocks.findAll().then(stocks => {
            res.send({ 
                status_code: '200',
                status_message: 'Success',
                stocks 
            })
            
          })

    },

    updateStocksAPI: (req, res) => {

        const paramLocation = req.body.location_id;
        const paramProduct = req.body.product;
        const paramAdjustment = req.body.adjustment;
        const paramOperation = req.body.operation;

        const mysql = require('mysql');
        const con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "klikdaily"
        });

        con.connect(function(err) {
            if (err) throw err;
            const sql = "INSERT INTO trstocks (location_id,product,adjustment,operation) VALUES ('"+paramLocation+"','"+ paramProduct+"','"+ paramAdjustment+"','"+ paramOperation+"') ";

            const sql2 = " SELECT * FROM stocks WHERE location = '"+paramLocation+"' AND product = '"+paramProduct+"'  ";
            
            
            con.query(sql, function (err, result) {
                
                if (err) throw err;
                result;

            });
            
            con.query(sql2, function (err, result) {
                if (err) throw err;
                const currentStock = result[0].quantity;

                if ( currentStock === '0' && paramOperation === '-' ) {

                    res.send({ 
                        status_code: '200',
                        status_message: 'Stocks already empty'  
                    })

                } else if ( currentStock === '0' && paramOperation === '+' ) {
                    
                    latestStock = Number(paramAdjustment);
    
                    const sql3 = " UPDATE stocks SET quantity = '"+latestStock+"' WHERE location = '"+paramLocation+"' AND product = '"+paramProduct+"' AND quantity = '"+currentStock+"' ";
                    con.query(sql3, function (err, result) {
                        if (err) throw err;
                        res.send({ 
                            status_code: '200',
                            status_message: 'Success',
                            result  
                        })
                        
                    });

                }  else if ( currentStock !== '0' && paramOperation === '+' ) {
                    
                    latestStock = Number(currentStock) + Number(paramAdjustment);
    
                    const sql3 = " UPDATE stocks SET quantity = '"+latestStock+"' WHERE location = '"+paramLocation+"' AND product = '"+paramProduct+"' AND quantity = '"+currentStock+"' ";
                    con.query(sql3, function (err, result) {
                        if (err) throw err;
                        res.send({ 
                            status_code: '200',
                            status_message: 'Success',
                            result  
                        })
                        
                    });

                } else if ( currentStock !== '0' && paramOperation === '-' ) {

                    if ( Number(currentStock) <= Number(paramAdjustment) ) {
                        latestStock = "0";
                    } else {
                        latestStock = Number(currentStock) - Number(paramAdjustment);    
                    }
    
                    const sql3 = " UPDATE stocks SET quantity = '"+latestStock+"' WHERE location = '"+paramLocation+"' AND product = '"+paramProduct+"' AND quantity = '"+currentStock+"' ";
                    con.query(sql3, function (err, result) {
                        if (err) throw err;
                        res.send({ 
                            status_code: '200',
                            status_message: 'Success',
                            result  
                        })
                        
                    });

                } else {

                    res.send({ 
                        status_code: '200',
                        status_message: 'Failed to update data'  
                    })

                }
              
            });

        });

       

    },

};
