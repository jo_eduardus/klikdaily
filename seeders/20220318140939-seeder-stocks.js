'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    
      await queryInterface.bulkInsert('stocks', 
      [
        {
          
          location: 'A-1-1',
          quantity: '100',
          product: 'Indomie Goreng'
        },
        {
          
          location: 'A-1-2',
          quantity: '150',
          product: 'Teh Kotak'
        },
        {
          
          location: 'A-1-3',
          quantity: '50',
          product: 'Susu Kental Manis'
        },
        {
          
          location: 'A-1-4',
          quantity: '90',
          product: 'Pasta Gigi'
        },
        {
          
          location: 'A-1-5',
          quantity: '30',
          product: 'Tissue'
        },
      ], 
      
      {});
    
  },

  async down (queryInterface, Sequelize) {
   
     await queryInterface.bulkDelete('stocks', null, {});
     
  }
};
