'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trstocks extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  trstocks.init({
    location_id: DataTypes.STRING,
    product: DataTypes.STRING,
    adjustment: DataTypes.STRING,
    operation: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'trstocks',
  });
  return trstocks;
};