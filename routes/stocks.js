var express = require('express');
var router = express.Router();

const { indexStocksAPI, updateStocksAPI } = require('../controllers/stocksAPIController');

//view or read
router.get('/stocks', indexStocksAPI);

router.post('/adjustment', updateStocksAPI);

module.exports = router;
